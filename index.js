/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// First Activity 

function printUsers(){	
	let fullName = prompt("What is your Name?"); 
	let yourAge = prompt("How old are you?"); 
	let yourPlace = prompt("Where do you live?");
    alert("Thank you for your Inputs!");

    console.log(`Hello, ${fullName}`);
	console.log(`You are ${yourAge} years old`);
    console.log(`You live in ${yourPlace}`);
};

// console.log(friend1); // nothing
// console.log(friend2); // nothing

printUsers();


function favoriteBands() {
    console.log("1. The Beatles");
    console.log("2. Metallica");
    console.log("3. The Eagles");
    console.log("4. L'arc~en~Ciel");
    console.log("5. Eraserheads");
}

favoriteBands();

// End of First Activity

// Rotten Tomato Rating

let movie1 = 97;
let movie2 = 96;
let movie3 = 91;
let movie4 = 93;
let movie5 = 96;

// for Tomato Ratings
function tomatoRatings1() {
    let tomatoRatingsHundreds = ( movie1 );
    console.log("Rotten Tomatoes Rating: " + tomatoRatingsHundreds +"%");
}

function tomatoRatings2() {
    let tomatoRatingsHundreds = ( movie2 );
    console.log("Rotten Tomatoes Rating: " + tomatoRatingsHundreds +"%");
}

function tomatoRatings3() {
    let tomatoRatingsHundreds = ( movie3 );
    console.log("Rotten Tomatoes Rating: " + tomatoRatingsHundreds +"%");
}

function tomatoRatings4() {
    let tomatoRatingsHundreds = ( movie4 );
    console.log("Rotten Tomatoes Rating: " + tomatoRatingsHundreds +"%");
}

function tomatoRatings5() {
    let tomatoRatingsHundreds = ( movie5 );
    console.log("Rotten Tomatoes Rating: " + tomatoRatingsHundreds +"%");
}

function movieTitle() {
    console.log("1. The Godfather");
    tomatoRatings1();
    console.log("2. The Godfather, Part II");
    tomatoRatings2();
    console.log("3. Shawshank Redemption");
    tomatoRatings3();
    console.log("4. To Kill A Mockingbird");
    tomatoRatings4();
    console.log("5. Psycho");
    tomatoRatings5();
}

movieTitle();

// End of Tomato Rating

// Debug

function printFriends() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:"); 
    let friend2 = prompt("Enter your second friend's name:"); 
    let friend3 = prompt("Enter your third friend's name:");
    

    console.log("You are friends with:")
    console.log(friend1); 
    console.log(friend2); 
    console.log(friend3); 
};

printFriends()